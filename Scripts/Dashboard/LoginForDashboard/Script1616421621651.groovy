import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://www.cilsy.id')

WebUI.maximizeWindow()

WebUI.click(findTestObject('HomePage/masuk_button'))

WebUI.delay(2)

WebUI.setText(findTestObject('Authentication/LoginPage/email_input'), 'trustoim+test5@gmail.com')

WebUI.setEncryptedText(findTestObject('Authentication/LoginPage/password_input'), 'a1YqozJa/IlJvKoWMLQGHw==' //rahasia123
    )

WebUI.click(findTestObject('Authentication/LoginPage/remember_checkbox'))

WebUI.click(findTestObject('Authentication/LoginPage/masuk_button'))

WebUI.delay(3)

//WebUI.closeBrowser()

