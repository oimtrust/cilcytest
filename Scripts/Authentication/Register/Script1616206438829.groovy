import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

// how to install faker library in katalon studio 
// how to implement faker in katalon studio
// faker library support for java/groovy


WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.cilsy.id/')

WebUI.click(findTestObject('HomePage/daftar_button'))

WebUI.setText(findTestObject('Authentication/RegisterPage/username_input'), 'cilcytest5')

WebUI.setText(findTestObject('Authentication/RegisterPage/email_input'), 'trustoim+test5@gmail.com')

WebUI.setEncryptedText(findTestObject('Authentication/RegisterPage/password_input'), 'a1YqozJa/IlJvKoWMLQGHw==')

WebUI.setEncryptedText(findTestObject('Authentication/RegisterPage/password_confirmation_input'), 'a1YqozJa/IlJvKoWMLQGHw==')

WebUI.setText(findTestObject('Authentication/RegisterPage/full_name_input'), 'Cilcy Test 4')

WebUI.selectOptionByValue(findTestObject('Authentication/RegisterPage/tgl_lahir_select'), '3', true)

WebUI.selectOptionByValue(findTestObject('Authentication/RegisterPage/bln_lahir_select'), '2', true)

WebUI.selectOptionByValue(findTestObject('Authentication/RegisterPage/thn_lahir_select'), '1996', true)

WebUI.doubleClick(findTestObject('Authentication/RegisterPage/lokasi_input'))

WebUI.setText(findTestObject('Authentication/RegisterPage/lokasi_input'), 'Bengkulu')

WebUI.setText(findTestObject('Authentication/RegisterPage/role_input'), 'Pengajar Gabut')

WebUI.setText(findTestObject('Authentication/RegisterPage/instansi_input'), 'Cilcy')

WebUI.setText(findTestObject('Authentication/RegisterPage/no_telepon_input'), '081245678915')

WebUI.setText(findTestObject('Authentication/RegisterPage/minat_belajar_input'), 'Kalo niat')

WebUI.click(findTestObject('Authentication/RegisterPage/daftar_button'))
