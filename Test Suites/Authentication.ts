<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Authentication</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>17f6cc9e-368e-48de-8897-7c80138ce9fc</testSuiteGuid>
   <testCaseLink>
      <guid>397c45b5-3dab-4e5e-a6a6-a36ee90064e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Authentication/Register</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0aaefe79-daa7-4f97-a298-906c131240eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Authentication/Login</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
